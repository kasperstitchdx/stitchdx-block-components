<?php

namespace Stitchdx\WordPress\Blocks\NarwhalComponents;

if ( ! defined( 'ABSPATH' ) ) {
	return;
}

// Add actions.
add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\\enqueueBlockEditorAssets', 5 );

/**
 * Returns the block version number.
 *
 * @return string
 */
function version() {
	return '0.1';
}

add_action( 'init', __NAMESPACE__ . '\\registerScripts' );

function registerScripts() {
	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	wp_register_script( 'narwhal-block-components', assetUrl( "assets/js/components{$suffix}.js" ), [
		'wp-components',
		'wp-editor',
		'wp-element',
	], version(), true );
}

/**
 * Enqueues styles and scripts for the admin editor.
 */
function enqueueBlockEditorAssets() {
	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	wp_enqueue_style( 'narwhal-block-components', assetUrl( "assets/css/components{$suffix}.css" ), [], version() );
}

/**
 * Returns appropriate URL for block assets.
 *
 * @param $path
 *
 * @return mixed
 */
function assetUrl( $path ) {
	if ( defined( 'NARWHAL_BLOCKS_URL' ) ) {
		/**
		 * If NARWHAL_BLOCKS_URL is defined, use it. Should be full URL to the directory containing all the Narwhal
		 * blocks. In a composer setup, it would be the `narwhal-digital` namespace folder.
		 */
		return rtrim( NARWHAL_BLOCKS_URL, '/' ) . '/' . basename( __DIR__ ) . '/' . ltrim( $path, '/' );
	}

	return plugins_url( $path, __FILE__ );
}
