import {Component, Fragment} from '@wordpress/element';
import {__} from '@wordpress/i18n';

import classNames from 'classnames/dedupe';
import _ from 'lodash';

import {fallback, getClassNames} from './util';
import EmbedLoading from './embed-loading';
import EmbedPlaceholder from './embed-placeholder';
import EmbedPreview from './embed-preview';
import EmbedControls from "./embed-controls";

/**
 * Accepted attributes:
 * - url
 * - onUrlUpdate
 * - className - video embed specific class names, must be passed to video save content method.
 * - onClassNameUpdate (callback, gets className string)
 * - isSelected
 * - caption
 * - onCaptionChange
 * - supportsCaption - bool - default true
 */

class VideoEmbed extends Component {

    constructor() {
        super(...arguments);

        this.setCaption = this.setCaption.bind(this);
        this.setUrl = this.setUrl.bind(this);

        this.state = {
            editingURL: false,
            url: this.props.url,
        };

        if (undefined === this.props.supportsCaption) {
            this.props.supportsCaption = true;
        }

        if (this.props.preview) {
            this.setAttributesFromPreview();
        }
    }

    componentDidUpdate(prevProps) {
        const hasPreview = undefined !== this.props.preview;
        const hadPreview = undefined !== prevProps.preview;
        const previewChanged = prevProps.preview && this.props.preview && this.props.preview.html !== prevProps.preview.html;
        const switchedPreview = previewChanged || (hasPreview && !hadPreview);
        const switchedURL = this.props.url !== prevProps.url;

        if (switchedPreview || switchedURL) {
            if (this.props.cannotEmbed) {
                // Can't embed this URL, and we've just received or switched the preview.
                return;
            }
            this.setAttributesFromPreview();
        }
    }

    /***
     * Gets block attributes based on the preview and responsive state.
     *
     * @param {string} preview The preview data.
     * @param {boolean} allowResponsive Apply responsive classes to fixed size content.
     * @return {Object} Attributes and values.
     */
    getAttributesFromPreview(preview, allowResponsive = true) {
        const attributes = {};
        // Some plugins only return HTML with no type info, so default this to 'rich'.
        let {type = 'rich'} = preview;
        // If we got a provider name from the API, use it for the slug, otherwise we use the title,
        // because not all embed code gives us a provider name.
        const {html, provider_name: providerName} = preview;
        const providerNameSlug = _.kebabCase(_.toLower('' !== providerName ? providerName : ''));

        if (html) {
            attributes.type = type;
            attributes.providerNameSlug = providerNameSlug;
        }

        attributes.className = getClassNames(html, this.props.className, allowResponsive);

        return classNames(attributes.className, {
            [`is-type-${attributes.type}`]: attributes.type,
            [`is-provider-${attributes.providerNameSlug}`]: attributes.providerNameSlug,
        });
    }

    setAttributesFromPreview() {
        const {onClassNameUpdate, preview, themeSupportsResponsive} = this.props;
        onClassNameUpdate(this.getAttributesFromPreview(preview, themeSupportsResponsive));
    }

    setCaption(caption) {
        const {onCaptionChange} = this.props;
        onCaptionChange(caption);
    }

    setUrl(event) {
        if (event) {
            event.preventDefault();
        }
        const {url} = this.state;
        const {onUrlUpdate} = this.props;
        this.setState({editingURL: false});
        onUrlUpdate(url);
    }

    render() {
        const {editingURL, url} = this.state;
        const {cannotEmbed, caption, className, fetching, isSelected, preview, supportsCaption, tryAgain} = this.props;

        if (fetching) {
            return (<EmbedLoading />);
        }

        if (!preview || cannotEmbed || editingURL) {
            return (
                <EmbedPlaceholder
                    cannotEmbed={cannotEmbed}
                    className={className}
                    fallback={() => fallback(url, this.props.onReplace)}
                    icon=""
                    label={__('Embed video', 'narwhal')}
                    onSubmit={this.setUrl}
                    onChange={(event) => this.setState({url: event.target.value})}
                    tryAgain={tryAgain}
                    value={url}
                />
            );
        }

        return (
            <Fragment>
                <EmbedPreview
                    caption={caption}
                    supportsCaption={supportsCaption}
                    onCaptionChange={this.setCaption}
                    className={className}
                    preview={preview}
                    url={url}
                    isSelected={isSelected}
                />
                <EmbedControls
                    showEditButton={isSelected}
                    switchBackToURLInput={() => this.setState({editingURL: true})}
                />
            </Fragment>
        );
    }

}

export default VideoEmbed;
