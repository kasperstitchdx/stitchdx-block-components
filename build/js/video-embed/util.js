import {createBlock} from '@wordpress/blocks';
import {renderToString} from '@wordpress/element';
import classNames from 'classnames/dedupe';
import {ASPECT_RATIOS} from './constants';

/**
 * Fallback behaviour for unembeddable URLs.
 * Creates a paragraph block containing a link to the URL, and calls `onReplace`.
 *
 * @param {string}   url       The URL that could not be embedded.
 * @param {function} onReplace Function to call with the created fallback block.
 */
export function fallback(url, onReplace) {
    const link = <a href={url}>{url}</a>;
    onReplace(
        createBlock('core/paragraph', {content: renderToString(link, {})})
    );
}

/**
 * Returns class names with any relevant responsive aspect ratio names.
 *
 * @param {string}  html               The preview HTML that possibly contains an iframe with width and height set.
 * @param {string}  existingClassNames Any existing class names.
 * @param {boolean} allowResponsive    If the responsive class names should be added, or removed.
 * @return {string} Deduped class names.
 */
export function getClassNames(html, existingClassNames = '', allowResponsive = true) {
    if (!allowResponsive) {
        // Remove all of the aspect ratio related class names.
        const aspectRatioClassNames = {
            'wp-has-aspect-ratio': false,
        };
        for (let ratioIndex = 0; ratioIndex < ASPECT_RATIOS.length; ratioIndex++) {
            const aspectRatioToRemove = ASPECT_RATIOS[ratioIndex];
            aspectRatioClassNames[aspectRatioToRemove.className] = false;
        }
        return classNames(
            existingClassNames,
            aspectRatioClassNames
        );
    }

    const previewDocument = document.implementation.createHTMLDocument('');
    previewDocument.body.innerHTML = html;
    const iframe = previewDocument.body.querySelector('iframe');

    // If we have a fixed aspect iframe, and it's a responsive embed block.
    if (iframe && iframe.height && iframe.width) {
        const aspectRatio = (iframe.width / iframe.height).toFixed(2);
        // Given the actual aspect ratio, find the widest ratio to support it.
        for (let ratioIndex = 0; ratioIndex < ASPECT_RATIOS.length; ratioIndex++) {
            const potentialRatio = ASPECT_RATIOS[ratioIndex];
            if (aspectRatio >= potentialRatio.ratio) {
                return classNames(
                    existingClassNames,
                    {
                        [potentialRatio.className]: allowResponsive,
                        'wp-has-aspect-ratio': allowResponsive,
                    }
                );
            }
        }
    }

    return existingClassNames;
}