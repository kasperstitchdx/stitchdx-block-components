import {__} from '@wordpress/i18n';
import {RichText} from '@wordpress/editor';
import classNames from 'classnames';

export default ({caption, className, imgAlt, imgId, imgSrc, url}) => {
    if (!url) {
        return null;
    }

    return (
        <figure className={classNames('wp-block-embed', className)}>
            <div className="wp-block-embed__wrapper">
                {`\n${url}\n` /* URL needs to be on its own line. */}
                {imgSrc && (
                    <div className="wp-block-embed__narwhal-overlay">
                        <img
                            src={imgSrc}
                            alt={imgAlt}
                            className={`wp-image-${imgId} wp-block-embed__narwhal-overlay-image`}
                            data-image-id={imgId}
                        />
                        <button type="button" className="wp-block-embed__narwhal-play-button"
                                aria-label={__('Play', 'narwhal')}>
                            <span className="wp-block-embed__narwhal-play-button-label">{__('Play', 'narwhal')}</span>
                        </button>
                    </div>
                )}
            </div>
            {!RichText.isEmpty(caption) && <RichText.Content tagName="figcaption" value={caption} />}
        </figure>
    );
};
