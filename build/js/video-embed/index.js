import {compose} from '@wordpress/compose';
import {withSelect, withDispatch} from '@wordpress/data';

import VideoEmbedEdit from './edit';
import VideoEmbedSave from './save';

const VideoEmbed = compose(
    withSelect((select, ownProps) => {
        const {url} = ownProps;
        const core = select('core');
        const {getEmbedPreview, isPreviewEmbedFallback, isRequestingEmbedPreview, getThemeSupports} = core;
        const preview = undefined !== url && getEmbedPreview(url);
        const previewIsFallback = undefined !== url && isPreviewEmbedFallback(url);
        const fetching = undefined !== url && isRequestingEmbedPreview(url);
        const themeSupports = getThemeSupports();
        // The external oEmbed provider does not exist. We got no type info and no html.
        const badEmbedProvider = !!preview && undefined === preview.type && false === preview.html;
        // Some WordPress URLs that can't be embedded will cause the API to return
        // a valid JSON response with no HTML and `data.status` set to 404, rather
        // than generating a fallback response as other embeds do.
        const wordpressCantEmbed = !!preview && preview.data && preview.data.status === 404;
        const validPreview = !!preview && !badEmbedProvider && !wordpressCantEmbed;
        const cannotEmbed = undefined !== url && (!validPreview || previewIsFallback);
        return {
            preview: validPreview ? preview : undefined,
            fetching,
            themeSupportsResponsive: themeSupports['responsive-embeds'],
            cannotEmbed,
        };
    }),
    withDispatch((dispatch, ownProps) => {
        const {url} = ownProps;
        const coreData = dispatch('core/data');
        const tryAgain = () => {
            coreData.invalidateResolution('core', 'getEmbedPreview', [url]);
        };
        return {
            tryAgain,
        };
    })
)(VideoEmbedEdit);

VideoEmbed.Content = VideoEmbedSave;

export {VideoEmbed};
