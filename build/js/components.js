import {createSliderEditComponent, createSliderSaveComponent} from './slider';
import {MediaEmbed} from './media-embed';
import {MediaField} from './media-field';
import {VideoEmbed} from './video-embed';

const components = {
    createSliderEditComponent,
    createSliderSaveComponent,
    MediaEmbed,
    MediaField,
    VideoEmbed,
};

if (!window.narwhalWp) {
    window.narwhalWp = {};
}

window.narwhalWp.components = components;
