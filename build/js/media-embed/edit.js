import {__} from '@wordpress/i18n';
import {Fragment} from '@wordpress/element';
import {InspectorControls, BlockControls} from '@wordpress/editor';
import {Placeholder, Button, PanelBody, ToolbarButton, Toolbar} from '@wordpress/components';
import {MediaField} from '../media-field';
import {VideoEmbed} from '../video-embed';

export const MediaEmbedEdit = (props) => {
    const {
        imageId,
        imageLabels = {
            title: __('Image', 'narwhal'),
            name: __('image', 'narwhal'),
        },
        imagePreviewRender,
        imageWrapperClass,
        isSelected,
        onSelectImage,
        onRemoveImage,
        onTypeChange,
        onVideoClassNameUpdate,
        onVideoUrlUpdate,
        type,
        videoClassName,
        videoUrl,
    } = props;

    const renderPlaceholder = () => {
        return (
            <Placeholder
                icon='media-default'
                label={__('Select Media Type', 'narwhal')}
            >
                <Button
                    isLarge
                    onClick={() => onTypeChange('video')}
                >
                    {__('Video with Overlay', 'narwhal')}
                </Button>
                &nbsp;&nbsp;
                <Button
                    isLarge
                    onClick={() => onTypeChange('image')}
                >
                    {__('Image Only', 'narwhal')}
                </Button>
            </Placeholder>
        );
    };

    const renderImage = () => {
        return (
            <MediaField
                isSelected={isSelected}
                labels={imageLabels}
                onRemove={onRemoveImage}
                onSelect={onSelectImage}
                previewRender={imagePreviewRender}
                value={imageId}
                wrapperClassName={imageWrapperClass}
            />
        );
    };

    const renderVideo = () => {
        return (
            <Fragment>
                <VideoEmbed
                    className={videoClassName}
                    isSelected={isSelected}
                    onClassNameUpdate={onVideoClassNameUpdate}
                    onUrlUpdate={onVideoUrlUpdate}
                    supportsCaption={false}
                    url={videoUrl}
                />
                <InspectorControls>
                    <PanelBody title={__('Overlay Image', 'narwhal')}>
                        <MediaField
                            isSelected={isSelected}
                            labels={imageLabels}
                            onRemove={onRemoveImage}
                            onSelect={onSelectImage}
                            previewRender={imagePreviewRender}
                            value={imageId}
                        />
                    </PanelBody>
                </InspectorControls>
            </Fragment>
        );
    };

    const renderContent = () => {
        if (type === 'image') {
            return renderImage();
        } else if (type === 'video') {
            return renderVideo();
        }

        return renderPlaceholder();
    };

    const oppositeType = () => {
        if (type === 'image') {
            return 'video';
        }

        return 'image';
    };

    return (
        <Fragment>
            {renderContent()}
            {type && (<BlockControls>
                <Toolbar>
                    <ToolbarButton
                        icon={`format-${oppositeType()}`}
                        onClick={() => onTypeChange(oppositeType())}
                        title={'video' !== type ? __('Use a video instead', 'narwhal') : __('Use just an image instead', 'narwhal')}
                    />
                </Toolbar>
            </BlockControls>)}
        </Fragment>
    );
};