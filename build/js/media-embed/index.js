import {MediaEmbedEdit as MediaEmbed} from './edit';
import {MediaEmbedSave} from './save';

MediaEmbed.Content = MediaEmbedSave;

export {MediaEmbed};
