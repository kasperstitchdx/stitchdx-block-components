import {VideoEmbed} from '../video-embed';
import classNames from 'classnames';

export const MediaEmbedSave = (props) => {
    const {
        imageAlt,
        imageClassName,
        imageId,
        imageSrc,
        imageWrapperClassName,
        type,
        videoClassName,
        videoProviderNameSlug,
        videoType,
        videoUrl,
        videoWrapperClassName,
    } = props;

    if (type === 'image' && imageSrc) {
        return (
            <div className={classNames(imageWrapperClassName)}>
                <img
                    alt={imageAlt}
                    data-image-id={imageId || ''}
                    className={classNames(imageClassName, {
                        [`wp-image-${imageId}`]: imageId,
                    })}
                    src={imageSrc}
                />
            </div>
        );
    }

    if (type === 'video' && videoUrl) {
        return (
            <div className={videoWrapperClassName}>
                <VideoEmbed.Content
                    className={videoClassName}
                    imgAlt={imageAlt}
                    imgId={imageId}
                    imgSrc={imageSrc}
                    providerSlug={videoProviderNameSlug}
                    type={videoType}
                    url={videoUrl}
                />
            </div>
        );
    }

    return null;
};
