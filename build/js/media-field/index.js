import {MediaPlaceholder, MediaUpload} from '@wordpress/editor';
import {Toolbar, IconButton} from '@wordpress/components';
import {Component, Fragment} from '@wordpress/element';

export class MediaField extends Component {

    constructor() {
        super(...arguments);
        this.state = {
            hovering: false
        };
    };

    render() {
        const {
            isSelected,
            labels = {},
            onSelect,
            onRemove,
            previewRender,
            value,
            allowedTypes = ['image'],
            wrapperClassName,
        } = this.props;

        return (
            <Fragment>
                {value ? (
                    <div className={`${wrapperClassName} narwhal-block-components__media-field__wrapper`}
                         style={{position: 'relative'}} onMouseEnter={() => this.setState({hovering: true})}
                         onMouseLeave={() => this.setState({hovering: false})}>
                        <div className='narwhal-components__media-field__toolbar' style={{
                            position: 'absolute',
                            top: '0',
                            left: '0',
                            right: '0',
                            display: this.state.hovering ? 'block' : 'none'
                        }}>
                            <Toolbar>
                                <MediaUpload
                                    allowedTypes={allowedTypes}
                                    onSelect={onSelect}
                                    render={({open}) => (<IconButton icon='edit' onClick={open} />)}
                                    value={value}
                                />
                                {
                                    onRemove && (
                                        <IconButton icon='trash' onClick={onRemove} />
                                    )
                                }
                            </Toolbar>
                        </div>

                        {previewRender()}
                    </div>
                ) : isSelected && (
                    <div className={`${wrapperClassName}`}>
                        <MediaPlaceholder
                            allowedTypes={allowedTypes}
                            labels={labels}
                            onSelect={onSelect}
                            value={value}
                        />
                    </div>
                )}
            </Fragment>
        );
    }
}