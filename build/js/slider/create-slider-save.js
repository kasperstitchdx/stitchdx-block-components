import {InnerBlocks} from '@wordpress/editor';
import classNames from 'classnames';

export function createSliderSaveComponent(
    {
        componentClassName = '',
    }
) {
    return ({attributes, className}) => {
        const {align, carouselOptions} = attributes;

        return (
            <div className={classNames(className, componentClassName, {
                [`align${align}`]: align,
            })}>
                <div
                    className={`${componentClassName}__inner`}
                    data-slick={JSON.stringify(carouselOptions)}
                >
                    <InnerBlocks.Content />
                </div>
            </div>
        );
    }
}
