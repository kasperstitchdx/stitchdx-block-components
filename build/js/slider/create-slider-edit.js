import {__} from '@wordpress/i18n';
import {PanelBody, RangeControl, ToggleControl} from '@wordpress/components';
import {InspectorControls, InnerBlocks, BlockAlignmentToolbar, BlockControls} from '@wordpress/editor';
import {Component, createRef, Fragment} from '@wordpress/element';
import _ from 'lodash';
import $ from 'jquery';
import 'slick-carousel';

function blocksTemplate(count, component, options = {}) {
    return _.times(count, () => [component, options]);
}

export function createSliderEditComponent(
    {
        allowAlignmentChange = false,
        innerBlockType = 'core/paragraph',
        innerBlockOptions = {},
        slickEditorOptions = {},
        maxSlides = 10,
        minSlides = 1,
        validAlignments = [],
    }
) {
    return class SliderEdit extends Component {

        constructor() {
            super(...arguments);

            /*this.initializeSlick = this.initializeSlick.bind(this);
            this.destroySlick = this.destroySlick.bind(this);
            this.slick = null;*/

            this.container = createRef();
        }

        /*componentDidMount() {
            this.initializeSlick();
        }

        componentWillUnmount() {
            this.destroySlick();
        }

        componentWillUpdate(nextProps) {
            if (nextProps.attributes.slideCount !== this.props.attributes.slideCount && this.slick) {
                this.destroySlick();
            }
        }

        componentDidUpdate(prevProps, prevState) {
            if (prevProps.attributes.slideCount !== this.props.attributes.slideCount && !this.slick) {
                this.initializeSlick();
            }
        }

        initializeSlick() {
            setTimeout(() => {
                if ($(this.container.current).length) {
                    this.slick = $(this.container.current.querySelector('.editor-block-list__layout')).slick(this.slickOptions());
                }
            }, 100);
        }

        destroySlick() {
            if ($(this.container.current).length) {
                $(this.container.current.querySelector('.editor-block-list__layout')).slick('unslick');
                this.slick = null;
            }
        }

        slickOptions() {
            // Force infinite and autoplay to be false. They make for bad editing UI/X.
            return Object.assign({}, slickEditorOptions, {
                autoplay: false,
                draggable: false,
                infinite: false,
                swipe: false,
                touchMove: false,
            });
        }*/

        render() {
            const {className, setAttributes} = this.props;

            const {align, carouselOptions, slideCount} = this.props.attributes;

            const options = Object.assign({
                autoplay: false,
                autoplaySpeed: 3000,
                pauseOnHover: true,
            }, carouselOptions);

            const updateAlignment = (align) => setAttributes({align});

            const updateCarouselOption = (value, attrName) => {
                const carouselOptions = Object.assign({}, options);
                carouselOptions[attrName] = value;
                setAttributes({carouselOptions});
            };

            return (
                <Fragment>
                    <div className={className} ref={this.container}>
                        <InnerBlocks
                            template={blocksTemplate(slideCount, innerBlockType, innerBlockOptions)}
                            templateLock="all"
                        />
                    </div>
                    <InspectorControls>
                        <PanelBody title={__('Carousel Options', 'narwhal')}>
                            <RangeControl
                                label={__('Number of slides', 'narwhal')}
                                max={maxSlides}
                                min={minSlides}
                                onChange={slideCount => setAttributes({slideCount})}
                                value={slideCount}
                            />
                            <ToggleControl
                                label={__('Autoplay', 'narwhal')}
                                checked={options.autoplay}
                                onChange={() => updateCarouselOption(!options.autoplay, 'autoplay')}
                            />
                            {options.autoplay && (<RangeControl
                                label={__('Autoplay Speed', 'narwhal')}
                                help={__('Time between slide changes', 'narwhal')}
                                max={10}
                                min={1}
                                onChange={(value) => updateCarouselOption(value * 1000, 'autoplaySpeed')}
                                value={Math.round(options.autoplaySpeed / 1000)}
                            />)}
                            {options.autoplay && (<ToggleControl
                                label={__('Pause on hover', 'narwhal')}
                                checked={options.pauseOnHover}
                                onChange={() => updateCarouselOption(!options.pauseOnHover, 'pauseOnHover')}
                            />)}
                        </PanelBody>
                    </InspectorControls>
                    {allowAlignmentChange && (
                        <BlockControls>
                            <BlockAlignmentToolbar
                                controls={validAlignments}
                                onChange={updateAlignment}
                                value={align}
                            />
                        </BlockControls>
                    )}
                </Fragment>
            );
        }
    };

    /*return ({attributes, className, setAttributes}) => {
        const {align, carouselOptions, slideCount} = attributes;

        const options = Object.assign({
            autoplay: false,
            autoplaySpeed: 3000,
            pauseOnHover: true,
        }, carouselOptions);

        const updateAlignment = (align) => setAttributes({align});

        const updateCarouselOption = (value, attrName) => {
            const carouselOptions = Object.assign({}, options);
            carouselOptions[attrName] = value;
            setAttributes({carouselOptions});
        };

        return (
            <Fragment>
                <div className={className}>
                    <InnerBlocks
                        template={blocksTemplate(slideCount, innerBlockType, innerBlockOptions)}
                        templateLock="all"
                    />
                </div>
                <InspectorControls>
                    <PanelBody title={__('Carousel Options', 'narwhal')}>
                        <RangeControl
                            label={__('Number of slides', 'narwhal')}
                            max={maxSlides}
                            min={minSlides}
                            onChange={slideCount => setAttributes({slideCount})}
                            value={slideCount}
                        />
                        <ToggleControl
                            label={__('Autoplay', 'narwhal')}
                            checked={options.autoplay}
                            onChange={() => updateCarouselOption(!options.autoplay, 'autoplay')}
                        />
                        {options.autoplay && (<RangeControl
                            label={__('Autoplay Speed', 'narwhal')}
                            help={__('Time between slide changes', 'narwhal')}
                            max={10}
                            min={1}
                            onChange={(value) => updateCarouselOption(value * 1000, 'autoplaySpeed')}
                            value={Math.round(options.autoplaySpeed / 1000)}
                        />)}
                        {options.autoplay && (<ToggleControl
                            label={__('Pause on hover', 'narwhal')}
                            checked={options.pauseOnHover}
                            onChange={() => updateCarouselOption(!options.pauseOnHover, 'pauseOnHover')}
                        />)}
                    </PanelBody>
                </InspectorControls>
                {allowAlignmentChange && (
                    <BlockControls>
                        <BlockAlignmentToolbar
                            controls={validAlignments}
                            onChange={updateAlignment}
                            value={align}
                        />
                    </BlockControls>
                )}
            </Fragment>
        );
    };*/
}